package colruyt.pcrs;

import colruyt.pcrs.utillibs.WebUser;
import colruyt.pcrsejb.bo.enrolment.EnrolmentBo;
import colruyt.pcrsejb.bo.role.RoleBo;
import colruyt.pcrsejb.bo.team.TeamBo;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.facade.TeamFacade;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class MyTeamView implements Serializable {
    @EJB
    private TeamFacade teamFacade;

    @Inject
    private WebUser webUser;

    private List<TeamBo> teamsOfManager;

    private EnrolmentBo clickedEnrolment = new EnrolmentBo();
    private List<RoleBo> possibleRoles = new ArrayList<>();

    @PostConstruct
    private void fillList() {
        teamsOfManager = teamFacade.getTeamsOfManager(webUser.getUser());
    }

    public List<TeamBo> getTeamsOfManager() {
        return teamsOfManager;
    }

    public void setTeamsOfManager(List<TeamBo> teamsOfManager) {
        this.teamsOfManager = teamsOfManager;
    }

    public EnrolmentBo getClickedEnrolment() {
        return clickedEnrolment;
    }

    public void setClickedEnrolment(EnrolmentBo clickedEnrolment) {
        this.clickedEnrolment = clickedEnrolment;
    }

    public void editEnrolment() {
        EnrolmentBo enrolmentBo = null;

        //Role aanpassen
        //UserPrivilege aanpassen

    }

    public List<RoleBo> getPossibleRoles() {
        return possibleRoles;
    }

    public void setPossibleRoles(List<RoleBo> possibleRoles) {
        this.possibleRoles = possibleRoles;
    }
}

