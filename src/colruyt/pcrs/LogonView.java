package colruyt.pcrs;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import colruyt.pcrsejb.bo.privileges.PrivilegeBo;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.bo.userPrivilege.PrivilegeTypeBo;
import colruyt.pcrsejb.bo.userPrivilege.UserPrivilegeBo;
import colruyt.pcrsejb.facade.UserFacade;

@Named
@SessionScoped
public class LogonView implements Serializable {
	@EJB
	private UserFacade userFacade;

	private String email;
	private String password;
	
	public void login() {

		FacesContext context = FacesContext.getCurrentInstance();
		UserBo user = userFacade.getUserByEmail(email);
		if (user != null && this.password.equals(user.getPassword())) {
			context.getExternalContext().getSessionMap().put("user", user);
			try {
				context.getExternalContext().redirect("profile.xhtml");
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			// Send an error message on Login Failure
			context.addMessage(null, new FacesMessage("Authentication Failed. Check username or password."));

		}
	}

	public void logout() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().invalidateSession();
		try {
			context.getExternalContext().redirect("login.xhtml");
		} catch (IOException e) {
			e.printStackTrace(); 
		}
	}

	public void logoutURL() {
		logout();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}