package colruyt.pcrs.filters;

import colruyt.pcrsejb.bo.userPrivilege.PrivilegeTypeBo;

public class DirectorPrivilegeFilter extends PrivilegeFilter {

	public DirectorPrivilegeFilter() {
		super();
		setMinimumPrivilege(PrivilegeTypeBo.DIRECTOR);
	}

}
