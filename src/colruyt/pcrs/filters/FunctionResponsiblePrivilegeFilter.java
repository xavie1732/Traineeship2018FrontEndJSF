package colruyt.pcrs.filters;

import colruyt.pcrsejb.bo.userPrivilege.PrivilegeTypeBo;

public class FunctionResponsiblePrivilegeFilter extends PrivilegeFilter {

	public FunctionResponsiblePrivilegeFilter() {
		super();
		setMinimumPrivilege(PrivilegeTypeBo.FUNCTIONRESPONSIBLE);
	}

}
