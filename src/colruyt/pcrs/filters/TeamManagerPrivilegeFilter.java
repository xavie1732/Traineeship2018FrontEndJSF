package colruyt.pcrs.filters;

import colruyt.pcrsejb.bo.userPrivilege.PrivilegeTypeBo;

public class TeamManagerPrivilegeFilter extends PrivilegeFilter {

	public TeamManagerPrivilegeFilter() {
		super();
		setMinimumPrivilege(PrivilegeTypeBo.TEAMMANAGER);
	}

}
