package colruyt.pcrs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import colruyt.pcrsejb.bo.competence.CompetenceBo;
import colruyt.pcrsejb.bo.competence.CraftCompetenceBo;
import colruyt.pcrsejb.bo.competence.RoleCompetenceBo;
import colruyt.pcrsejb.bo.function.FunctionBo;
import colruyt.pcrsejb.bo.role.RoleBo;
import colruyt.pcrsejb.facade.CompetenceFacade;
import colruyt.pcrsejb.facade.FunctionFacade;
@Named
@SessionScoped
public class CraftCompeteceView implements Serializable {

    private static final long serialVersionUID = 1L;
    @EJB
    private CompetenceFacade competenceFacade;
    //private CraftCompetenceBo craftCompetence= new CraftCompetenceBo();
    private List<CompetenceBo> craftCompetences = new ArrayList<>();
    @EJB
    private FunctionFacade functionFacade;
    private List<FunctionBo> functions;
    private List<RoleBo> roles;


    public List<CompetenceBo> getCompetences() {
        for(RoleBo rb : getRoles()){
            for(CompetenceBo rbb : rb.getCraftCompetenceBoList()){
                CraftCompetenceBo ccb = (CraftCompetenceBo) rbb;
                craftCompetences.add(ccb);
            }
        }
        //craftCompetences = competenceFacade.getAllCompetences();
        return craftCompetences;
    }

    public List<FunctionBo> getFunctions() {
        functions = functionFacade.getAllFunctionNames();

        return functions;
    }

    private List<RoleBo> getRoles(){
        roles = new ArrayList<>();
        for (FunctionBo fbo : this.getFunctions()){
            roles.addAll(fbo.getRoleBoSet());
        }
        return roles;
    }


}
