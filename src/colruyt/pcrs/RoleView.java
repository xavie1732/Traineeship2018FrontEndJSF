package colruyt.pcrs;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import colruyt.pcrsejb.bo.role.RoleBo;
import colruyt.pcrsejb.facade.RoleFacade;
@Named
@ViewScoped
public class RoleView implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
    private RoleFacade roleFacade;
    private RoleBo role = new RoleBo();
    private List<RoleBo> roles;

    public List<RoleBo> getRoles() {
        roles = roleFacade.getAllRoles();
        return roles;
    }


    public String addRole(){
        System.out.println(role.getName());
        roleFacade.saveRole(role);
        return "role";
    }


    public RoleBo getRole() {
        return role;
    }


}
