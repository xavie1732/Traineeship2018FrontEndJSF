package colruyt.pcrs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.bo.userPrivilege.PrivilegeTypeBo;
import colruyt.pcrsejb.bo.userPrivilege.UserPrivilegeBo;
import colruyt.pcrsejb.entity.user.User;
import colruyt.pcrsejb.facade.UserFacade;

@Named
@ViewScoped
public class UsersView implements Serializable{
	private static final long serialVersionUID = 1L;
	@EJB
	private UserFacade userFacade;
	private UserBo addedUser;
	private List<UserBo> users;
	private PrivilegeTypeBo[] privileges;

	@PostConstruct 
	private void fillusers() {
		users = userFacade.getAllUsers();
	}
	
	public List<PrivilegeTypeBo> getPrivilegeTypes() {
		List<PrivilegeTypeBo> privs = new ArrayList<PrivilegeTypeBo>();
		for (PrivilegeTypeBo pt : PrivilegeTypeBo.values()) {
			if (!(pt.equals(PrivilegeTypeBo.TEAMMEMBER) || pt.equals(PrivilegeTypeBo.FUNCTIONRESPONSIBLE))) {
				privs.add(pt);
			}
		}
		return privs;
	}

	public List<UserBo> getUsers() {
		return users;
	}
	
	public void setUsers(List<UserBo> users) {
		this.users = users;
	}

	public UserBo getAddedUser() {
		return addedUser;
	}

	public void setAddedUser(UserBo addedUser) {
		this.addedUser = addedUser;
		List<PrivilegeTypeBo> userPrivileges = new ArrayList<>();
		for (UserPrivilegeBo up : addedUser.getPrivilegeBoSet()) {
			if (up.isActive()) {
				userPrivileges.add(up.getPrivilegeType());
			}
		}
		privileges = new PrivilegeTypeBo[userPrivileges.size()];
		for (int i=0; i < userPrivileges.size(); i++) {
			privileges[i] = userPrivileges.get(i);
		}
	}

	public void addUser() {
        // Add one new user to the table and to the database:
		HashSet<UserPrivilegeBo> privs = new HashSet<UserPrivilegeBo>();
		for(PrivilegeTypeBo pt : privileges) {
			privs.add(new UserPrivilegeBo(pt, true));
		}
		addedUser.setPrivilegeBoSet(privs);
		users.add(userFacade.saveUser(addedUser));
    }
	
	public void editUser() {
		UserBo u = null;
		for (UserBo user : users) {
			if (user.getId() == addedUser.getId()) {
				user.setFirstName(addedUser.getFirstName());
				user.setLastName(addedUser.getLastName());
				user.setCountry(addedUser.getCountry());
				user.setEmail(addedUser.getEmail());
				user.setPassword(addedUser.getPassword());

				HashSet<UserPrivilegeBo> privs = (HashSet<UserPrivilegeBo>) user.getPrivilegeBoSet();
				for(PrivilegeTypeBo pt : getPrivilegeTypes()) {
					UserPrivilegeBo p = null;
					for (UserPrivilegeBo privilege : privs) {
						if (privilege.getPrivilegeType().equals(pt)) {
							p = privilege;
						}
					}
					privs.remove(p);
				}
				for(PrivilegeTypeBo pt : privileges) {
					privs.add(new UserPrivilegeBo(pt, true));
				}
				user.setPrivilegeBoSet(privs);
				u = user;
			}
		}
		userFacade.saveUser(u); 
	}
	
	public void deleteUser() {
		UserBo u = null;
		for (UserBo user : users) {
			if (user.getId() == addedUser.getId()) {
				u = user;
			}
		}
		users.remove(u);
		userFacade.removeUser(addedUser);
	}
	
	public boolean hasActiveAdminPrivilege(UserBo userBo)
	{
		return userFacade.hasPrivilege(userBo, PrivilegeTypeBo.ADMINISTRATOR, true);
	}


	public PrivilegeTypeBo[] getPrivileges() {
		return privileges;
	}

	public void setPrivileges(PrivilegeTypeBo[] privileges) {
		this.privileges = privileges;
	}

	public void newUser() {
        addedUser = new UserBo(); 
    } 	
}

