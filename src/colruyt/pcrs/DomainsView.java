package colruyt.pcrs;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import colruyt.pcrsejb.bo.domain.DomainBo;
import colruyt.pcrsejb.facade.DomainFacade;

@Named
@ViewScoped
public class DomainsView implements Serializable{
	private static final long serialVersionUID = 1L;
	@EJB
	private DomainFacade domainFacade;
	private DomainBo addedDomain;
	private List<DomainBo> domains;

	@PostConstruct 
	private void filldomains() {
		domains = domainFacade.getAllDomains();
	}
	
	public DomainBo getAddedDomain() {
		return addedDomain;
	}
	
	public void setDomains(List<DomainBo> domains) {
		this.domains = domains;
	}

	public List<DomainBo> getDomains() {
		return domains;
	}

	public void setAddedDomain(DomainBo addedDomain) {
		this.addedDomain = addedDomain;
	}

	public void addDomain() {
        // Add one new domain to the table and to the database:
		domains.add(domainFacade.saveDomain(addedDomain));
    }
	
	public void editDomain() {
		DomainBo d = null;
		for (DomainBo domain : domains) {
			if (domain.getId() == addedDomain.getId()) {
				domain.setName(addedDomain.getName());
				d = domain;
			}
		}
		domainFacade.saveDomain(d); 
	}
	
	public void deleteDomain() {
		DomainBo d = null;
		for (DomainBo domain : domains) {
			if (domain.getId() == addedDomain.getId()) {
				d = domain;
			}
		}
		domains.remove(d);
		domainFacade.removeDomain(addedDomain);
	}

	public void newDomain() {
        addedDomain = new DomainBo(); 
    } 	
}

