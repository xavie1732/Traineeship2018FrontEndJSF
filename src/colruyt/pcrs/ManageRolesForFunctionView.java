package colruyt.pcrs;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ValueChangeEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import colruyt.pcrs.utillibs.WebUser;
import colruyt.pcrsejb.bo.role.RoleBo;
import colruyt.pcrsejb.facade.RoleFacade;

/**
 * View for selecting roles that belong to a certain function
 * done by the function responsible
 */
@Named
@ViewScoped
public class ManageRolesForFunctionView implements Serializable{

	@EJB
    private RoleFacade roleFacade;
    private RoleBo addRoleBo = new RoleBo();
    Map<RoleBo, Boolean> checked = new HashMap<>();

    @Inject
    private WebUser webUser;

    /**
     * Mandatory no-args constructor
     */
    public ManageRolesForFunctionView() {
    }

    @PostConstruct
    public void setup(){
        // TODO Get the functionBo from the logged in function responsible
        // and pass this to get the enabled roles for his function
        // hardcoded for now
        for (RoleBo rb : roleFacade.getAllRoles()) {
            checked.put(rb, false);
        }

        webUser.getFunctionBo();
        for (RoleBo rb : webUser.getFunctionBo().getRoleBoSet()){
            checked.replace(rb, true);
        }
    }

    public void setChecked(Map<RoleBo, Boolean> checked) {
        this.checked = checked;
    }

    public Map<RoleBo, Boolean> getChecked() {
        return this.checked;
    }

    public void onRoleClickListener(){

    }
}
