package colruyt.pcrs;

import colruyt.pcrs.utillibs.WebUser;
import colruyt.pcrsejb.bo.function.FunctionBo;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.facade.TeamFacade;
import colruyt.pcrsejb.facade.UserFacade;
import colruyt.pcrsejb.util.exceptions.bl.UserIsNotMemberOfTeamException;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;




@Named
public class UserProfileview implements Serializable {

	private static final long serialVersionUID = 1L;

	private String newpassword, repeatpassword, currentpass;

	@EJB
	private UserFacade userfac;
	@EJB
	private TeamFacade teamFacade;
	
	@Inject
	private WebUser webuser;

	public String getTeamLeaderNaam() {
		try {
		  return this.teamFacade.getManager(this.getUser()).getFullName();
		}
		catch(Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			return  context.getApplication().evaluateExpressionGet(context, "#{msgs['error.noteam']}",
					String.class);
		}
	}

	public String getNewpassword() {
		return newpassword;
	}

	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}

	public String getRepeatpassword() {
		return repeatpassword;
	}

	public void setRepeatpassword(String repeatpassword) {
		this.repeatpassword = repeatpassword;
	}

	public String getCurrentpass() {
		return currentpass;
	}

	public void setCurrentpass(String currentpass) {
		this.currentpass = currentpass;
	}

	public UserFacade getUserfac() {
		return userfac;
	}

	public void setUserfac(UserFacade userfac) {
		this.userfac = userfac;
	}

	public FunctionBo getFunction() {
		return this.webuser.getFunctionBo();
	}

	public UserProfileview() {
		super();
	}




	public String getTeamnaam(){
		try {
		return this.teamFacade.getTeam(this.getUser()).getName(); 
		}
		catch(UserIsNotMemberOfTeamException e) {
			
			FacesContext context = FacesContext.getCurrentInstance();
			return  context.getApplication().evaluateExpressionGet(context, "#{msgs['error.noteam']}",
					String.class);
		}
	}

	public UserBo getUser() {
		return this.webuser.getUser();
	}

	public void setUser(UserBo user) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().put("user", user);
	}

	public void changePass() {

		if (this.isCurrentPassword() && this.isSamePassword()) {

			this.getUser().setPassword(newpassword);	

			this.setUser(userfac.saveUser(this.getUser()));
		}
	}

	public boolean isSamePassword() {
		if (!this.newpassword.equals(this.repeatpassword)) {

			FacesContext context = FacesContext.getCurrentInstance();
			String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['error.passmissmatch']}",
					String.class);
			FacesMessage myFacesMessage = new FacesMessage(message);

			context.addMessage(null, myFacesMessage);
			context.validationFailed();
			return  false;
		}
		else {
			return true;
		}
	}
	
	public boolean isCurrentPassword() {
		if(!this.currentpass.equals(this.getUser().getPassword())) {
			   FacesContext context = FacesContext.getCurrentInstance();
			   String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['error.wrongpassword']}", String.class);
			   FacesMessage myFacesMessage = new FacesMessage(message);
			   context.addMessage(null,myFacesMessage);

			   context.validationFailed();


			return false;
		} else {

			return true;
		}
	}
}