package colruyt.pcrs.utillibs;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import colruyt.pcrsejb.bo.function.FunctionBo;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.bo.userPrivilege.UserPrivilegeBo;
import colruyt.pcrsejb.facade.FunctionFacade;

@Named
@SessionScoped
public class WebUser implements Serializable{
	private FunctionBo functionBo;
	@EJB
	FunctionFacade functionFacade;

	public WebUser() {
	}
	
	@PostConstruct
	private void getFunctionOfUser() {
		functionBo = new FunctionBo();
		functionBo.setId(1);
		functionBo = functionFacade.getFunction(functionBo);
	}

	public UserBo getUser() {
		FacesContext context = FacesContext.getCurrentInstance();
		return ((UserBo) context.getExternalContext().getSessionMap().get("user")); 
	}
	
	public boolean hasUser() {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getExternalContext().getSessionMap().containsKey("user");
	}
	
	public boolean hasPrivilege(String privilegeType) {
		boolean hasPrivilege = false;
		for (UserPrivilegeBo privilege : getUser().getPrivilegeBoSet()) {
			if (privilege.isActive() && privilege.getPrivilegeType().getFullName().equalsIgnoreCase(privilegeType)) {
				hasPrivilege = true;
			}
		}
		return hasPrivilege;
	}

	public FunctionBo getFunctionBo() {
		return functionBo;
	}

	public void setFunctionBo(FunctionBo functionBo) {
		this.functionBo = functionBo;
	}
}
