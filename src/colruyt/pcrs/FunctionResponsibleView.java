package colruyt.pcrs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import colruyt.pcrsejb.bo.function.FunctionBo;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.bo.userPrivilege.FunctionResponsibleUserPrivilegeBo;
import colruyt.pcrsejb.bo.userPrivilege.PrivilegeTypeBo;
import colruyt.pcrsejb.facade.FunctionFacade;
import colruyt.pcrsejb.facade.UserFacade;
import colruyt.pcrsejb.util.exceptions.bl.FunctionResponsibleAlreadyAssignedException;
import org.primefaces.event.SelectEvent;

@Named
@ViewScoped
public class FunctionResponsibleView implements Serializable{


    private Map<FunctionResponsibleUserPrivilegeBo, UserBo> functionResponsibleMap;
    private List<FunctionBo> functionList;
    private List<UserBo> matchingUsers;



    private String userName;
    private int functionId;
    private String country;

    private boolean userSelected = false;
    private UserBo selectedUser = new UserBo();
    private String email;

    private FunctionResponsibleUserPrivilegeBo selectedPrivilege;


    @EJB
    private UserFacade userFacade;
    @EJB
    private FunctionFacade functionFacade;


    public FunctionResponsibleView() {

    }

    @PostConstruct
    public void setUp(){
        this.setFunctionResponsibleMap(userFacade.getAllFunctionResponsibles());
        this.setFunctionList(functionFacade.getAllFunctionNames());
    }

    public Map<FunctionResponsibleUserPrivilegeBo, UserBo> getFunctionResponsibleMap() {
        return this.functionResponsibleMap;
    }

    public void setFunctionResponsibleMap(Map<FunctionResponsibleUserPrivilegeBo, UserBo> functionResponsibleList) {
        this.functionResponsibleMap = functionResponsibleList;
    }


//    public void addFunctionResponsible(){
//        List<UserBo> matchingUsers = userFacade.getUsersByShortName(userName);
//        FunctionBo selectedFunction = this.getFunctionFromList(functionId);
//        if (matchingUsers.size() > 1) {
//            // multiple users matching
//            // TODO multiple users matching the string
//        } else if (matchingUsers.size() == 1) {
//            // only 1 user matching
//            try {
//                userFacade.addPrivilegeForUser(
//                        new FunctionResponsibleUserPrivilegeBo(PrivilegeTypeBo.FUNCTIONRESPONSIBLE, true, selectedFunction, country),
//                        matchingUsers.get(0));
//            } catch (FunctionResponsibleAlreadyAssignedException e) {
//                e.printStackTrace();
//            }
//        } else {
//            // no users matching
//            // TODO No matching users
//        }
//
//    }



    public FunctionBo getFunctionFromList(int id) {
        FunctionBo functionBo = null;
        for (FunctionBo fbo : functionList) {
            if (fbo.getId() == getFunctionId()) {
                functionBo = fbo;
            }
        }
        return functionBo;
    }

    public void newFunctionResponsible(){
        System.out.println("newFunctionResponsible()");
        userName = null;
        functionId = 0;
        country = null;
    }

    public List<String> completeText(String query) {
        System.out.println("CompleteText()");
        List<String> emails = new ArrayList<>();
        // short name = 5 letters
        if (query.length() == 5) {
            matchingUsers = userFacade.getUsersByShortName(query);
            for(UserBo userBo: matchingUsers) {
                emails.add(userBo.getEmail());
            }
        }

        return emails;
    }

    public void onItemSelect(SelectEvent event) {
        System.out.println("Inside listener");
        System.out.println("Selected user " + selectedUser);
    }



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getFunctionId() {
        return functionId;
    }

    public void setFunctionId(int functionId) {
        this.functionId = functionId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<FunctionBo> getFunctionList() {
        return this.functionList;
    }

    public void setFunctionList(List<FunctionBo> functionList) {
        this.functionList = functionList;
    }

    public FunctionResponsibleUserPrivilegeBo getSelectedPrivilege() {
        return selectedPrivilege;
    }

    public void setSelectedPrivilege(FunctionResponsibleUserPrivilegeBo selectedPrivilege) {
        this.selectedPrivilege = selectedPrivilege;
    }

    public boolean isUserSelected() {
        return userSelected;
    }

    public void setUserSelected(boolean userSelected) {
        this.userSelected = userSelected;
    }

    public UserBo getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(UserBo selectedUser) {
        this.selectedUser = selectedUser;
    }

    public void editUser(){

    }

    public void deleteUser(){

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
