package colruyt.pcrs;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import colruyt.pcrsejb.bo.function.FunctionBo;
import colruyt.pcrsejb.facade.FunctionFacade;

@Named
@ViewScoped
public class FunctionsView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private FunctionFacade functionFacade;
	private FunctionBo functionBo = new FunctionBo();
	private List<FunctionBo> functions;
	private HashSet<String> operatingUnitNames = new HashSet<>();
	
	public FunctionBo getFunctionBo() {
		return functionBo;
	}
	public void setFunctionBo(FunctionBo functionBo) {
		this.functionBo = functionBo;
	}
	public List<FunctionBo> getFunctions() {
		functions = functionFacade.getAllFunctionNames();
		return functions;
	}
	public void setFunctions(List<FunctionBo> functions) {
		this.functions = functions;
	}
	public HashSet<String> getOperatingUnitNames() {
        for(FunctionBo f : getFunctions()){
            operatingUnitNames.add(f.getOperatingUnitBo().getOperatingUnitName());
        }
		return operatingUnitNames;
	}
	public void setOperatingUnitNames(HashSet<String> operatingUnitNames) {
		this.operatingUnitNames = operatingUnitNames;
	}
	
	public void newFunction(){
		functionBo = new FunctionBo();
	}
	
	public void editFunction(){
		functionFacade.saveFunction(functionBo);
	}

}
